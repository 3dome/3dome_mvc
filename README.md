This repository contains the html source files needed to run the web-based viewer and some sample data.  

Instructions to run viewer locally:

1. Make sure you have Flask python framework installed. You can install it using command *pip install flask*
2. Run the python program viewapp.py
3. Use any modern browser to navigate to localhost:5000/index.html